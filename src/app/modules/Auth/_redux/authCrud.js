import axios from "axios";


//Unused when using SSO
export const LOGIN_URL = "http://192.168.1.15:8000/api/authentication";

export function login(username, password) {
  return axios.post(LOGIN_URL, { username, password});
}

export const getUserByToken = () => {
  // Authorization head should be fulfilled in interceptor.
  return axios.post(`http://192.168.1.15:8000/api/whoami`);
}

