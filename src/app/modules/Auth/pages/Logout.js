import React, {Component} from "react";
import {connect} from "react-redux";
import {Redirect} from "react-router-dom";
import {LayoutSplashScreen} from "../../../../_metronic/layout";
import * as auth from "../_redux/authRedux";
import axios from 'axios';

class Logout extends Component {
  componentDidMount() {
    const {REACT_APP_SSO_URL} = process.env;
    const wnd = window.open(`${REACT_APP_SSO_URL}logout`);
   
    setTimeout(() => {
      this.props.logout();
      wnd.close();
    }, 100);
    
  }

  render() {
    const { hasAuthToken } = this.props;
    document.cookie = "username=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
    return hasAuthToken ? <LayoutSplashScreen /> : <Redirect to="/auth/login" />;
  }
}

export default connect(
  ({ auth }) => ({ hasAuthToken: Boolean(auth.authToken) }),
  auth.actions
)(Logout);
