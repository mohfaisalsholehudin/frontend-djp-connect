/* eslint-disable react-hooks/exhaustive-deps */

import React, {useEffect, useState } from "react";
import axios from "axios";
import { useSelector } from "react-redux";
import {Card,
  CardBody,
  CardHeader,} from '../../../src/_metronic/_partials/controls'
import { Modal, Button } from "react-bootstrap";
 

export function DashboardPage() {

  const {user} = useSelector(state => state.auth)
  const [role, setRole] = useState([]);
  const [permission, setPermission] = useState([]);
  const [modul, setModul] = useState([]);
  const [apps, setApps] = useState([]);

  
  const [openModalPermission, setOpenModalPermission] = useState(false);
  const [openModalModul, setOpenModalModul] = useState(false);
  const [openModalApps, setOpenModalApps] = useState(false);
  const [permissionData, setPermissionData] = useState([]);
  const [modulData, setModulData] = useState([]);
  const [appsData, setAppsData] = useState([]);
  const [dataApps, setDataApps] = useState([]);



  const [modulBool, setModulBool] = useState(false);
  const [permBool, setPermBool] = useState(false);
  const [appsBool, setAppsBool] = useState(false)


  const getRoles = async (roles) => {
      return await axios.get(`https://127.0.0.1:8000/api/roles?nama=${roles}`);
  }
  const getPermissions = async (permissions) => {
    return await axios.get(`https://127.0.0.1:8000${permissions}`);
  }

  const getModul = async (modul) => {
    return await axios.get(`https://127.0.0.1:8000${modul}`);
  }

  const getApps = async (apps) => {
    return await axios.get(`https://127.0.0.1:8000${apps}`);
  }

const perm = role.map((data)=>{
  return data.permissions
})

console.log(user.pegawai['@id'])

  useEffect(() => {
    user.roles.map((roles) => {
      getRoles(roles)
      .then(({data}) => {
          setRole(data['hydra:member'])
      })
      .catch((e)=> {
        console.log(e)
      })
    })

  }, []);

//   useEffect(() => {
//     perm.map((data)=> {
//             data.map((data)=> {
//             console.log(data)
//           })
//   })
// },[role])

  
//   useEffect(() => {
//     perm.map((data)=> {
//       data.map((data)=> {
//       getPermissions(data)
//       .then(({data})=>{
//         setPermission(data)
//         // getModul(data.modul)
//         // .then(({data})=>{
//         //     setModul(data)
//         //     getApps(data.aplikasi)
//         //     .then(({data})=>{
//         //       setApps(data);
//         //     })
//         //     .catch((e)=>{
//         //       console.log(e)
//         //     })
//       //   })
//       //   .catch((e)=> {
//       //     console.log(e)
//       //   })
//       })
//       .catch((e)=>{
//         console.log(e)
//       })
//       })
//     })

//     // perm.map((data)=> {
//     //   data.map((data)=> {
//     //   getPermissions(data)
//     //   .then(({data})=>{
//     //     getModul(data.modul)
//     //     .then(({data})=>{
//     //         setModul(data)
//     //     //     getApps(data.aplikasi)
//     //     //     .then(({data})=>{
//     //     //       setApps(data);
//     //     //     })
//     //     //     .catch((e)=>{
//     //     //       console.log(e)
//     //     //     })
//     //     })
//     //     .catch((e)=> {
//     //       console.log(e)
//     //     })
//     //   })
//     //   .catch((e)=>{
//     //     console.log(e)
//     //   })
//     //   })
//     // })

//     // perm.map((data)=> {
//     //   data.map((data)=> {
//     //   getPermissions(data)
//     //   .then(({data})=>{
//     //     getModul(data.modul)
//     //     .then(({data})=>{
//     //         getApps(data.aplikasi)
//     //         .then(({data})=>{
//     //           setApps(data);
//     //         })
//     //         .catch((e)=>{
//     //           console.log(e)
//     //         })
//     //     })
//     //     .catch((e)=> {
//     //       console.log(e)
//     //     })
//     //   })
//     //   .catch((e)=>{
//     //     console.log(e)
//     //   })
//     //   })
//     // })
 



//  },[role]);

 const getDataPermission = async (data) => {
     await axios.get(`https://127.0.0.1:8000${data}`)
      .then(({data})=>{
          setPermissionData(data)
          setModulData(data.modul)
          setModulBool(true)
      })
 }

 const getDataModul = async (data) => {
   await axios.get(`https://127.0.0.1:8000${data}`)
   .then(({data})=>{
      setAppsData(data)
      setAppsBool(true)

   })
 }

 const getDataApps = async (data) => {
   await axios.get(`https://127.0.0.1:8000${data}`)
   .then(({data})=>{
    setDataApps(data)
 })
 }


 const showRole =() => {
   return role.map((data, index)=>(
        <div key={index} className="d-flex align-items-center mb-10">
          <label className="checkbox checkbox-lg checkbox-light-success checkbox-single flex-shrink-0 m-0 mx-4">
              {/* <span></span> */}
              </label>
            <div key={index} className="d-flex flex-column flex-grow-1 font-weight-bold">
              {/* <p key={data.id} className="text-dark text-hover-primary mb-1 font-size-lg">{data.nama}</p> */}
              <button aria-haspopup="true" aria-expanded="false" type="button" className="btn btn-light btn-sm font-size-sm font-weight-bolder text-dark-75 " onClick={()=>setPermBool(true)}>{data.nama}</button>
           <br/>
            </div>
        </div> 
   ))
 }

 const showPermission = () => {
  return perm.map((data)=>(
    data.map((data, index)=>(
        <div key={index} className="d-flex align-items-center mb-10">
        <label className="checkbox checkbox-lg checkbox-light-success checkbox-single flex-shrink-0 m-0 mx-4">
           </label>
        <div key={index} className="d-flex flex-column flex-grow-1 font-weight-bold">
          {/* <p key={data} className="text-dark  mb-1 font-size-lg">{data}</p> */}
        <button aria-haspopup="true" aria-expanded="false" value={data} type="button" className="btn btn-light btn-sm font-size-sm font-weight-bolder text-dark-75 " onClick={(e)=>handleOpenModal(e)}>{data}</button>
           <br/>
         </div>
   </div> 
  
    ))
  ))
 }

 const showModul =() => {
  return modulData.map((data, index)=>(
       <div key={index} className="d-flex align-items-center mb-10">
         <label className="checkbox checkbox-lg checkbox-light-success checkbox-single flex-shrink-0 m-0 mx-4">
             </label>
             <div key={index} className="d-flex flex-column flex-grow-1 font-weight-bold">
              <button aria-haspopup="true" aria-expanded="false" value={data} type="button" className="btn btn-light btn-sm font-size-sm font-weight-bolder text-dark-75 " onClick={(e)=>handleOpenModalModul(e)}>{data}</button>
              <br/>
         </div>
      </div> 
  ))
}

const showApps = () => {
  return (
    <div key={appsData.id} className="d-flex align-items-center mb-10">
      <label className="checkbox checkbox-lg checkbox-light-success checkbox-single flex-shrink-0 m-0 mx-4">
          </label>
          <div key={appsData.id} className="d-flex flex-column flex-grow-1 font-weight-bold">
           <button aria-haspopup="true" aria-expanded="false" value={appsData.aplikasi} type="button" className="btn btn-light btn-sm font-size-sm font-weight-bolder text-dark-75 " onClick={(e)=>handleOpenModalApps(e)}>{appsData.aplikasi}</button>
           <br/>
      </div>
   </div> 
)
}


 const handleOpenModal = (e) => {
  getDataPermission(e.target.value);
  setOpenModalPermission(true);
 }

 const handleCloseModal = () => {
   setOpenModalPermission(false);
 }

 const handleOpenModalModul = (e) => {
  getDataModul(e.target.value);
  setOpenModalModul(true);
 }

 const handleCloseModalModul = () => {
   setOpenModalModul(false);
 }

 const handleOpenModalApps = (e) => {
  getDataApps(e.target.value);
  setOpenModalApps(true);
 }

 const handleCloseModalApps = () => {
   setOpenModalApps(false);
 }

 
  return (
    <div className="container">
    <div className="row">
    <div className="col-xxl-8 order-2 order-xxl-1">
        <div className="card card-custom card-stretch gutter-b">
          <div className="card-header border-0">
            <h3 className="card-title font-weight-bolder text-dark">Detail User</h3>
          </div>
          <div className="card-body pt-2">
          <div className="py-9">
            <div className="d-flex align-items-center justify-content-between mb-2">
              <span className="font-weight-bold mr-2">Nama:</span>
              <span className="text-muted text-hover-primary">{user.pegawai.nama}</span>
            </div>
            <div className="d-flex align-items-center justify-content-between mb-2">
              <span className="font-weight-bold mr-2">IP SIKKA:</span>
              <span className="text-muted text-hover-primary">{user.pegawai.nip9}</span>
            </div>
            <div className="d-flex align-items-center justify-content-between mb-2">
              <span className="font-weight-bold mr-2">NIP:</span>
              <span className="text-muted text-hover-primary">{user.pegawai.nip18}</span>
            </div>
            <div className="d-flex align-items-center justify-content-between mb-2">
              <span className="font-weight-bold mr-2">Tanggal Lahir:</span>
              <span className="text-muted text-hover-primary">{user.pegawai.tanggalLahir}</span>
            </div>
            <div className="d-flex align-items-center justify-content-between mb-2">
              <span className="font-weight-bold mr-2">Tempat Lahir:</span>
              <span className="text-muted text-hover-primary">{user.pegawai.tempatLahir}</span>
            </div>
            <div className="d-flex align-items-center justify-content-between mb-2">
              <span className="font-weight-bold mr-2">Jenis Kelamin:</span>
              <span className="text-muted text-hover-primary">{user.pegawai.jenisKelamin.nama}</span>
            </div>
            <div className="d-flex align-items-center justify-content-between mb-2">
              <span className="font-weight-bold mr-2">Agama:</span>
              <span className="text-muted text-hover-primary">{user.pegawai.agama.nama}</span>
            </div>
          </div>
          </div>
           
        </div>

      </div>
    <div className="col-lg-6 col-xxl-4 order-1 order-xxl-1">
        <div className="card card-custom card-stretch gutter-b">
          <div className="card-header border-0">
            <h3 className="card-title font-weight-bolder text-dark">Roles</h3>
          </div>
          <div className="card-body pt-2">
          {showRole()}
          </div>
           
        </div>

      </div>

      {permBool? <div className="col-lg-6 col-xxl-4 order-1 order-xxl-2">
        <div className="card card-custom card-stretch gutter-b">
          <div className="card-header border-0">
            <h3 className="card-title font-weight-bolder text-dark">Permission</h3>
          </div>
          <div className="card-body pt-2">
          {showPermission()}
          </div>
          
        </div>

      </div> : ''}
      
      
      {modulBool? <div className="col-lg-6 col-xxl-4 order-1 order-xxl-2">
        <div className="card card-custom card-stretch gutter-b">
          <div className="card-header border-0">
            <h3 className="card-title font-weight-bolder text-dark">Modul</h3>
          </div>
          <div className="card-body pt-2">
          {showModul()}
          </div>
          
        </div>

      </div> : ''}
      {appsBool? <div className="col-lg-6 col-xxl-4 order-1 order-xxl-2">
        <div className="card card-custom card-stretch gutter-b">
          <div className="card-header border-0">
            <h3 className="card-title font-weight-bolder text-dark">Applications</h3>
          </div>
          <div className="card-body pt-2">
          {showApps()}
          </div>
          
        </div>

      </div> : ''}
      
      

    </div>


    <Modal show={openModalPermission} onHide={()=>handleCloseModal()}>
        <Modal.Header closeButton>
        <Modal.Title>Permission Name</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div className="container">
            <div className="row">
                <label className="checkbox checkbox-lg checkbox-light-success checkbox-single flex-shrink-0 m-0 mx-4">
                Permission Name: 
                </label>
                  <div  className="d-flex flex-column flex-grow-1 font-weight-bold">
                    <p className="text-dark  mb-1 font-size-lg">{permissionData.nama}</p>
                  </div>
            </div>
          </div>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={()=>handleCloseModal()}>
            Close
          </Button>
          
        </Modal.Footer>
    </Modal>
    <Modal show={openModalModul} onHide={()=>handleCloseModalModul()}>
        <Modal.Header closeButton>
        <Modal.Title>Modul Name</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div className="container">
            <div className="row">
                <label className="checkbox checkbox-lg checkbox-light-success checkbox-single flex-shrink-0 m-0 mx-4">
                Modul Name: 
                </label>
                  <div  className="d-flex flex-column flex-grow-1 font-weight-bold">
                    <p className="text-dark  mb-1 font-size-lg">{appsData.nama}</p>
                  </div>
            </div>
          </div>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={()=>handleCloseModalModul()}>
            Close
          </Button>
          
        </Modal.Footer>
    </Modal>
    <Modal show={openModalApps} onHide={()=>handleCloseModalApps()}>
        <Modal.Header closeButton>
        <Modal.Title>Application Name</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div className="container">
            <div className="row">
                <label className="checkbox checkbox-lg checkbox-light-success checkbox-single flex-shrink-0 m-0 mx-4">
                Application Name: 
                </label>
                  <div  className="d-flex flex-column flex-grow-1 font-weight-bold">
                    <p className="text-dark  mb-1 font-size-lg">{dataApps.nama}</p>
                  </div>
            </div>
          </div>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={()=>handleCloseModalApps()}>
            Close
          </Button>
          
        </Modal.Footer>
    </Modal>
    
    
    </div>
  );
}

